package com.example.formativa1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class CiudadAdapter extends ArrayAdapter<Ciudades>{

    public CiudadAdapter(@NonNull Context context, @NonNull ArrayList<Ciudades> ciudad) {
        super(context, 0, ciudad);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
       Ciudades c = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.lista, parent, false);
        }

        TextView nombre = (TextView) convertView.findViewById(R.id.txtNombre);
        TextView pais = (TextView) convertView.findViewById(R.id.txtPais);
        TextView region = (TextView) convertView.findViewById(R.id.txtRegion);
        TextView zonaH = (TextView) convertView.findViewById(R.id.txtZonaHoraria);
        TextView latitud = (TextView) convertView.findViewById(R.id.txtLatitud);
        TextView longitud = (TextView) convertView.findViewById(R.id.txtLongitud);

        nombre.setText(c.getNombre());
        pais.setText(c.getPais());
        region.setText(c.getRegion());
        zonaH.setText(c.getZonaH());
        latitud.setText(c.getLatitud());
        longitud.setText(c.getLongitud());

        return convertView;
    }
}
