package com.example.formativa1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BuscarCiudad extends AppCompatActivity {

    TextView nombre;
    Button buscarC;
    ArrayList<Ciudades> ciudades = new ArrayList<>();
    String url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_ciudad);

        nombre = findViewById(R.id.txtNombreBuscar);
        buscarC = findViewById(R.id.btnBuscarCiudadNombre);

        buscarC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                url = "https://devru-latitude-longitude-find-v1.p.rapidapi.com/latlon.php?location="+nombre.getText().toString();
                requestDatos(url);
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                Intent i = new Intent(getApplicationContext(), ListarCiudades.class);
                                i.putParcelableArrayListExtra("ciudades", ciudades);
                                startActivity(i);
                                ciudades.clear();
                            }
                        }, 3000);

            }
        });

    }

    public void requestDatos(String url){
        RequestQueue cola = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parserJson(response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Error en la conexion", Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("x-rapidapi-key", "6ed433d250msh473509b0a98c43dp1024c5jsnefc04d2bf70e");
                return headers;
            }
        };
        cola.add(jsonObjectRequest);
    }

    public void parserJson(JSONObject response){
        try {
            JSONArray ciudad = response.getJSONArray("Results");
            for (int i = 0 ; i<ciudad.length(); i++) {
                JSONObject c = ciudad.getJSONObject(i);
                String nombre = c.getString("name");
                String pais = c.getString("c");
                String region = c.getString("tz");
                String zonah = c.getString("tzs");
                String latitud = c.getString("lat");
                String longitud = c.getString("lon");

                Ciudades ciu = new Ciudades(nombre, pais, region, zonah, latitud, longitud);
                ciudades.add(ciu);
            }

        } catch (JSONException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_global, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.optInicio) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }
}
