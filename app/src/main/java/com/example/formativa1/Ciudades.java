package com.example.formativa1;

import android.os.Parcel;
import android.os.Parcelable;

public class Ciudades implements Parcelable {

    String nombre;
    String pais;
    String region;
    String zonaH;
    String latitud;
    String longitud;

    public Ciudades(String nombre, String pais, String region, String zonaH, String latitud, String longitud) {
        this.nombre = nombre;
        this.pais = pais;
        this.region = region;
        this.zonaH = zonaH;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    protected Ciudades(Parcel in) {
        nombre = in.readString();
        pais = in.readString();
        region = in.readString();
        zonaH = in.readString();
        latitud = in.readString();
        longitud = in.readString();
    }

    public static final Creator<Ciudades> CREATOR = new Creator<Ciudades>() {
        @Override
        public Ciudades createFromParcel(Parcel in) {
            return new Ciudades(in);
        }

        @Override
        public Ciudades[] newArray(int size) {
            return new Ciudades[size];
        }
    };

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getZonaH() {
        return zonaH;
    }

    public void setZonaH(String zonaH) {
        this.zonaH = zonaH;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    @Override
    public String toString() {
        return "Ciudades{" +
                "nombre='" + nombre + '\'' +
                ", pais='" + pais + '\'' +
                ", region='" + region + '\'' +
                ", zonaH='" + zonaH + '\'' +
                ", latitud='" + latitud + '\'' +
                ", longitud='" + longitud + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nombre);
        dest.writeString(pais);
        dest.writeString(region);
        dest.writeString(zonaH);
        dest.writeString(latitud);
        dest.writeString(longitud);
    }
}
